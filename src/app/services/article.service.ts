import { Injectable } from '@angular/core';
import { Article } from '../models/article';
import { articles } from "../../data/articles.json"

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  constructor() { }

  public getAllArticles(): Article[] {
    return articles.sort((a, b) => {
      return <any>new Date(b.publishDate) - <any>new Date(a.publishDate);
    });
  }

  public getArticle(id: number): Article{
    let articles = this.getAllArticles();
    let article = articles.find(a => a.id === id);
    return article;
  }
}