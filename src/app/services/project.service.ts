import { Injectable } from '@angular/core';
import { Project } from "../models/project";
import { projects } from "../../data/projects.json";

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor() { }

  public getAllProjects(): Project[] {
    return projects.sort((a, b) => {
      return b.id - a.id;
    });
  }

  public getProject(id: number): Project{
    let projects = this.getAllProjects();
    let project = projects.find(a => a.id === id);
    return project;
  }
}
