import { Routes } from "@angular/router";
import { LandingComponent } from 'src/app/pages/landing/landing.component';
import { ResumeComponent } from 'src/app/pages/resume/resume.component';
import { ProjectsComponent } from 'src/app/pages/projects/projects.component';
import { ProjectDetailsComponent } from 'src/app/pages/project-details/project-details.component';
import { BlogComponent } from 'src/app/pages/blog/blog.component';
import { ArticleComponent } from 'src/app/pages/article/article.component';

export const routes: Routes = [
  {
    path: "",
    component: LandingComponent
  },
  {
    path: "resume",
    component: ResumeComponent
  },
  {
    path: "projects",
    component: ProjectsComponent
  },
  {
    path: "projects/:id",
    component: ProjectDetailsComponent
  },
  {
    path: "blog",
    component: BlogComponent
  },
  {
    path: "blog/:id",
    component: ArticleComponent
  }
]