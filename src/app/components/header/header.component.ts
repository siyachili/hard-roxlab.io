import { Component, OnInit } from '@angular/core';
import { GlobalFunctions } from "../../utilities/global-functions";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
