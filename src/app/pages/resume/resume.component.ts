import { Component, OnInit } from '@angular/core';
import { ExternalScriptLoader } from 'src/app/utilities/ext-script-loader';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-resume',
  templateUrl: './resume.component.html',
  styleUrls: ['./resume.component.css']
})
export class ResumeComponent implements OnInit {

  constructor(private titleService: Title) { }

  ngOnInit() {
    ExternalScriptLoader.loadScript();
    this.titleService.setTitle("Resume - Rasedur Rahman Roxy")
  }

}
