import { Component, OnInit } from '@angular/core';
import { ExternalScriptLoader } from 'src/app/utilities/ext-script-loader';
import { Title, Meta } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { Project } from 'src/app/models/project';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.css']
})
export class ProjectDetailsComponent implements OnInit {

  public project: Project;

  constructor(private titleService: Title,
    private metaService: Meta,
    private activatedRoute: ActivatedRoute,
    private projectService: ProjectService) { }

  ngOnInit() {
    ExternalScriptLoader.loadScript();
    let id = +this.activatedRoute.snapshot.paramMap.get("id");
    this.project = this.projectService.getProject(id);
    this.titleService.setTitle(this.project.name + " - Rasedur Rahman Roxy");

    this.metaService.updateTag({name: "title", content: this.project.name });
    this.metaService.updateTag({name: "description", content: this.project.shortDetails});
    this.metaService.updateTag({name: "og:image", content: this.project.demoImageUrls[0]});
    this.metaService.updateTag({name: "twitter:image", content: this.project.demoImageUrls[0]});
  }

}
