import { Component, OnInit } from '@angular/core';
import { ExternalScriptLoader } from 'src/app/utilities/ext-script-loader';
import { Title } from '@angular/platform-browser';
import { Article } from 'src/app/models/article';
import { ArticleService } from 'src/app/services/article.service';
import { Project } from 'src/app/models/project';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  public latestArticles: Article[] = [];
  public latestProjects: Project[] = [];

  constructor(private titleService: Title,
    private articleService: ArticleService,
    private projectService: ProjectService) { }

  ngOnInit() {
    ExternalScriptLoader.loadScript();
    this.titleService.setTitle("Rasedur Rahman Roxy - Home");

    this.latestArticles = this.articleService.getAllArticles().slice(0, 3);
    this.latestProjects = this.projectService.getAllProjects().slice(0, 3);
  }

}
