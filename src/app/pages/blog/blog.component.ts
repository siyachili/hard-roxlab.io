import { Component, OnInit } from '@angular/core';
import { ExternalScriptLoader } from 'src/app/utilities/ext-script-loader';
import { Title } from '@angular/platform-browser';
import { ArticleService } from 'src/app/services/article.service';
import { Article } from 'src/app/models/article';

@Component({
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {

  public articles: Article[] = [];

  constructor(private titleService: Title,
    private articleService: ArticleService) { }

  ngOnInit() {
    ExternalScriptLoader.loadScript();
    this.titleService.setTitle("Blog - Rasedur Rahman Roxy")

    this.articles = this.articleService.getAllArticles();
  }

}
