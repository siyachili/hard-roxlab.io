import { Component, OnInit } from '@angular/core';
import { ExternalScriptLoader } from 'src/app/utilities/ext-script-loader';
import { Title } from '@angular/platform-browser';
import { Project } from 'src/app/models/project';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {

  public projects: Project[] = [];

  constructor(private titleService: Title,
    private projectService: ProjectService) { }

  ngOnInit() {
    ExternalScriptLoader.loadScript();
    this.titleService.setTitle("Projects - Rasedur Rahman Roxy");

    this.projects = this.projectService.getAllProjects();
  }

}
