export class ExternalScriptLoader {
    static loadScript() {
        window.scrollTo(0, 0);
        var scripts = document.getElementsByTagName("script")
        var dynamicScripts = [
            "./assets/js/main.js"
        ];

        for (var i = 0; i < dynamicScripts.length; i++) {
            for (var j = 0; j < scripts.length; ++j) {
                if (scripts[j].getAttribute('src') != null && scripts[j].getAttribute('src') === dynamicScripts[i]) {
                    scripts[j].remove();
                }
            }
            let node = document.createElement('script');
            node.src = dynamicScripts[i];
            node.type = 'text/javascript';
            node.async = false;
            node.charset = 'utf-8';
            document.getElementsByTagName('head')[0].appendChild(node);
        }
    }
}