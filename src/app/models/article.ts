export class Article {
    public id: number;
    public title: string;
    public publishDate: string;
    public coverImageUrl: string;
    public category: string;
    public tags: string[];
    public markdownFileName: string;
}