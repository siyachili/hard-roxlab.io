export class Project{
    public id: number;
    public name: string;
    public shortDetails: string;
    public demoImageUrls: string[];
    public type: string;
    public tags: string[];
    public markdownFileName: string;
}