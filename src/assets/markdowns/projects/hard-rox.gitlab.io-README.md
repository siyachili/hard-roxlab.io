# hard-rox.gitlab.io

**hard-rox.gitlab.io** is an angular web app for my blog & portfolio. Built with Angular 8. Here I used GitLab Pages with GitLab CI/CD pipeline to deploy the site in gitlab.io & markdown file to present articles & project details. Many thanks to Rafid Reza Nabil for photographs & ColorLib for their open sourced template. Any suggestions, isses will be wolcomed.

## Getting Started

To getting started with this project:
1. Clone the repository
2. Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
3. Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

### Prerequisites

Things have to installed to continue...
1. [Node.js](https://nodejs.org/en/download/).
Pretty much it.


## Built With

* [Node.js](https://nodejs.org/en/download/) - The framework used
* [Visual Studio Code](https://code.visualstudio.com/download) - IDE
* [ngx-markdown](https://www.npmjs.com/package/ngx-markdown) - Database Management Studio
* [Git](https://git-scm.com/downloads) - For versioning.
* GitLab Pages
* GitLab CI/CD

## License

This project is licensed under the MIT License.

## Acknowledgments

* Hat tip to anyone whose code was used
* [Rafid Reza Nabil](https://www.facebook.com/rafidreza1) for photographs.
* [ColorLib](https://colorlib.com) for their opensourced template.
